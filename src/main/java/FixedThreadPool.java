import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

public class FixedThreadPool implements ThreadPool {
    private Set<PoolWorker> threads;
    private final LinkedBlockingQueue<Runnable> queue;
    private boolean flag;
    private int nThreads;

    public FixedThreadPool(int nThreads) {
        this.nThreads = nThreads;
        queue = new LinkedBlockingQueue();
        threads = new HashSet<>(nThreads);
        for (int i = 0; i < nThreads; i++) {
            threads.add(new PoolWorker());
        }
    }

    public void start() {
        threads.forEach(Thread::start);
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            queue.notify();
        }
    }

    public void stop(){
        flag = true;
    }


    private class PoolWorker extends Thread {
        public void run() {
            Runnable task;
            while (!interrupted()) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                    task = queue.poll();
                }

                try {
                    task.run();
                } catch (RuntimeException e) {
                }

                if (queue.isEmpty() && flag){
                    interrupt();
                }
            }
        }
    }
}