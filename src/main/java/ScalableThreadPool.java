import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;

public class ScalableThreadPool implements ThreadPool {
    private final int nThreadsMin;
    private final int mThreadsMax;

    private Set<Worker> threads;
    private final LinkedBlockingQueue<Runnable> queue;

    public ScalableThreadPool(int nThreadsMin, int maxJobCount) {
        this.nThreadsMin = nThreadsMin;
        this.mThreadsMax = maxJobCount;
        queue = new LinkedBlockingQueue();
        threads = new HashSet<>();
        for (int i = 0; i < nThreadsMin; i++) {
            threads.add(new Worker());
        }

    }

    public void start() {
        threads.forEach(Thread::start);
    }

    public void execute(Runnable task) {
        synchronized (queue) {
            queue.add(task);
            if (queue.size() > threads.size() && threads.size() < mThreadsMax) {
                Worker poolWorker = new Worker();
                threads.add(poolWorker);
                poolWorker.start();
            }
            queue.notify();
        }
    }



    private class Worker extends Thread {
        public void run() {
            Runnable task;
            while (!interrupted()) {
                synchronized (queue) {
                    while (queue.isEmpty()) {
                        try {
                            queue.wait();
                        } catch (InterruptedException e) {
                        }
                    }
                    task = queue.poll();
                }

                try {
                    task.run();
                } catch (RuntimeException e) {
                }

                if (queue.size() < threads.size() && queue.size() >= nThreadsMin) {
                    interrupt();
                    threads.remove(this);
                }
            }
        }
    }
}