public class Main {
    public static void main(String[] args) throws InterruptedException {
        ThreadPool threadPool = new ScalableThreadPool(2, 4);
        threadPool.start();
        for (int i = 0; i < 7; i++) {
            threadPool.execute(new Work(i));
        }

        Thread.sleep(30000);

        for (int i = 0; i < 7; i++) {
            threadPool.execute(new Work(i));
        }
    }
}

